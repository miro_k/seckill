package com.ruyuan.seckill.service.cartbuilder.impl;


import com.ruyuan.seckill.domain.Buyer;
import com.ruyuan.seckill.domain.enums.CartType;
import com.ruyuan.seckill.domain.enums.CheckedWay;
import com.ruyuan.seckill.domain.vo.CartVO;
import com.ruyuan.seckill.domain.vo.CartView;
import com.ruyuan.seckill.domain.vo.CouponVO;
import com.ruyuan.seckill.domain.vo.PriceDetailVO;
import com.ruyuan.seckill.service.cartbuilder.*;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

/**
 * 促销信息构建器
 */
@Slf4j
public class DefaultCartBuilder implements CartBuilder {


    /**
     * 购物车促销规则渲染器
     */
    private CartPromotionRenderer cartPromotionRenderer;

    /**
     * 购物车价格计算器
     */
    private CartPriceCalculator cartPriceCalculator;

    /**
     * 数据校验
     */
    private CheckDataRebderer checkDataRebderer;

    /**
     * 购物车sku渲染器
     */
    private CartSkuRenderer cartSkuRenderer;

    /**
     * 购物车优惠券渲染
     */
    private CartCouponRenderer cartCouponRenderer;

    private Buyer buyer;
    /**
     * 运费价格计算器
     */
    private CartShipPriceCalculator cartShipPriceCalculator;


    private List<CartVO> cartList;
    private PriceDetailVO price;
    private CartType cartType;
    private List<CouponVO> couponList;


    public DefaultCartBuilder(CartType cartType, CartSkuRenderer cartSkuRenderer, CartPromotionRenderer cartPromotionRenderer, CartPriceCalculator cartPriceCalculator, CheckDataRebderer checkDataRebderer) {
        this.cartType = cartType;
        this.cartSkuRenderer = cartSkuRenderer;
        this.cartPromotionRenderer = cartPromotionRenderer;
        this.cartPriceCalculator = cartPriceCalculator;
        this.checkDataRebderer = checkDataRebderer;
        cartList = new ArrayList<>();
    }

    public DefaultCartBuilder(CartType cartType, CartSkuRenderer cartSkuRenderer, CartPromotionRenderer cartPromotionRenderer, CartPriceCalculator cartPriceCalculator, CartCouponRenderer cartCouponRenderer, CartShipPriceCalculator cartShipPriceCalculator, CheckDataRebderer checkDataRebderer,Buyer buyer) {
        this.cartType = cartType;
        this.cartSkuRenderer = cartSkuRenderer;
        this.cartPromotionRenderer = cartPromotionRenderer;
        this.cartPriceCalculator = cartPriceCalculator;
        this.cartCouponRenderer = cartCouponRenderer;
        this.cartShipPriceCalculator = cartShipPriceCalculator;
        this.checkDataRebderer = checkDataRebderer;
        this.buyer = buyer;
        cartList = new ArrayList<>();
    }

    public Buyer getBuyer() {
        return buyer;
    }

    public void setBuyer(Buyer buyer) {
        this.buyer = buyer;
    }

    /**
     * 渲染sku
     */
    @Override
    public CartBuilder renderSku(CheckedWay way) {
        cartSkuRenderer.renderSku(this.cartList, cartType, buyer, way);
        return this;
    }

    /**
     * 带过滤器式的渲染sku<br/>
     * 可以过滤为指定条件{@link  CartSkuFilter}的商品<br/>
     *
     * @return
     * @see CartSkuFilter
     */
    @Override
    public CartBuilder renderSku(CartSkuFilter filter, CheckedWay way) {
        cartSkuRenderer.renderSku(this.cartList, filter, cartType, way);
        return this;
    }


    /**
     * 此步通过
     *
     * @return
     */
    @Override
    public CartBuilder renderPromotion() {
        cartPromotionRenderer.render(cartList,buyer);
        return this;
    }

    /**
     * 此步通过上一步的产出物
     * 来计算出价格:
     * {@link PriceDetailVO}
     *
     * @return
     */
    @Override
    public CartBuilder countPrice(Boolean includeCoupon) {

        this.price = cartPriceCalculator.countPrice(cartList,buyer, includeCoupon);
        return this;
    }


    /**
     * 调用运费模板来算出运费，只接应用到购物车的价格中
     *
     * @return
     */
    @Override
    public CartBuilder countShipPrice() {
        cartShipPriceCalculator.countShipPrice(cartList,buyer);
        return this;
    }

    /**
     * 此步读取出会员的可用优惠券，加入到购物车的couponList中
     *
     * @return
     */
    @Override
    public CartBuilder renderCoupon() {
        List<CouponVO> couponList = cartCouponRenderer.render(cartList,buyer);
        this.couponList = couponList;
        return this;
    }

    @Override
    public CartView build() {
        return new CartView(cartList, price, couponList);
    }

    @Override
    public CartBuilder checkData() {
        checkDataRebderer.checkData(cartList);
        return this;
    }
}
