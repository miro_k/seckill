package com.ruyuan.seckill.service.cartbuilder.impl;


import com.ruyuan.seckill.domain.vo.CartSkuVO;

/**
 * 购物车sku过滤器
 * 指定一定的条件，进行过滤购物车的sku。
 */
public interface CartSkuFilter {

    /**
     * 指定要过滤的条件
     * @param cartSkuVO 当前的sku，做要比对的对象
     * @return 返回true/false来决定是否过滤
     */
    boolean accept(CartSkuVO cartSkuVO);
}
