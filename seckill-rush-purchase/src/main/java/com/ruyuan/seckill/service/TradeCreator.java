package com.ruyuan.seckill.service;


import com.ruyuan.seckill.domain.vo.TradeVO;

public interface TradeCreator {

    /**
     * 检测配送范围
     *
     * @return
     */
    TradeCreator checkShipRange();

    /**
     * 检测商品合法性
     *
     * @return
     */
    TradeCreator checkGoods();

    /**
     * 检测促销活动合法性
     *
     * @return
     */
    TradeCreator checkPromotion();


    /**
     * 创建交易
     *
     * @return
     */
    TradeVO createTrade();


}
