package com.ruyuan.seckill.domain;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.ruyuan.seckill.domain.vo.CartSkuVO;
import com.ruyuan.seckill.domain.vo.PriceDetailVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@ApiModel(description = "购物车")
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class CartDO implements Serializable {


    private static final long serialVersionUID = 1466001652922300536L;

    @ApiModelProperty(value = "卖家id")
    private Integer sellerId;

    @ApiModelProperty(value = "选中的配送方式id")
    private Integer shippingTypeId;


    @ApiModelProperty(value = "选中的配送方式名称")
    private String shippingTypeName;


    @ApiModelProperty(value = "卖家店名")
    private String sellerName;


    @ApiModelProperty(value = "购物车重量")
    private Double weight;

    @ApiModelProperty(value = "购物车价格")
    private PriceDetailVO price;

    @ApiModelProperty(value = "购物车中的产品列表")
    private List<CartSkuVO> skuList;

    @ApiModelProperty(value = "赠品列表")
    private String giftJson;

    @ApiModelProperty(value = "赠送积分")
    private Integer giftPoint;

    @ApiModelProperty(value = "是否失效：0:正常 1:已失效")
    private Integer invalid;

    public CartDO() {
    }

    /**
     * 在构造器中初始化属主、产品列表、促销列表及优惠券列表
     */
    public CartDO(int sellerId, String sellerName) {

        this.sellerId = sellerId;
        this.sellerName = sellerName;
        price = new PriceDetailVO();
        skuList = new ArrayList<CartSkuVO>();
        giftJson = "";
        giftPoint = 0;
    }


    /**
     * 清空优惠信息功能，不清空优惠券
     */
    public void clearPromotion() {
        if (price != null) {
            price.clear();
        }
        giftJson = "";
        giftPoint = 0;
    }

    public String getGiftJson() {
        return giftJson;
    }

    public void setGiftJson(String giftJson) {
        this.giftJson = giftJson;
    }

    public Integer getSellerId() {
        return sellerId;
    }

    public void setSellerId(Integer sellerId) {
        this.sellerId = sellerId;
    }

    public Integer getShippingTypeId() {
        return shippingTypeId;
    }

    public void setShippingTypeId(Integer shippingTypeId) {
        this.shippingTypeId = shippingTypeId;
    }

    public String getShippingTypeName() {
        return shippingTypeName;
    }

    public void setShippingTypeName(String shippingTypeName) {
        this.shippingTypeName = shippingTypeName;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public PriceDetailVO getPrice() {
        return price;
    }

    public void setPrice(PriceDetailVO price) {
        this.price = price;
    }

    public List<CartSkuVO> getSkuList() {
        return skuList;
    }

    public void setSkuList(List<CartSkuVO> skuList) {
        this.skuList = skuList;
    }

    public Integer getGiftPoint() {
        return giftPoint;
    }

    public void setGiftPoint(Integer giftPoint) {
        this.giftPoint = giftPoint;
    }


    public Integer getInvalid() {
        return invalid;
    }


    public void setInvalid(Integer invalid) {
        this.invalid = invalid;
    }


}
