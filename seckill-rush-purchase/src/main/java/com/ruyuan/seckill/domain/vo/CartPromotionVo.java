package com.ruyuan.seckill.domain.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 活动的Vo
 */
@ApiModel(description = "购物车中活动Vo")
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class CartPromotionVo implements Serializable {


    private static final long serialVersionUID = 1867982008597357312L;


    @ApiModelProperty(value = "货品id")
    private Integer skuId;

    @ApiModelProperty(value = "活动id")
    private Integer promotionId;

    @ApiModelProperty(value = "活动工具类型")
    private String promotionType;

    @ApiModelProperty(value = "活动名称")
    private String promotionName;

    @ApiModelProperty(value = "是否选中参与这个活动,1为是 0为否")
    private Integer isCheck;

    @ApiModelProperty(value = "促销脚本", hidden = true)
    @JsonIgnore
    private String promotionScript;


    public Integer getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(Integer promotionId) {
        this.promotionId = promotionId;
    }

    public String getPromotionType() {
        return promotionType;
    }

    public void setPromotionType(String promotionType) {
        this.promotionType = promotionType;
    }

    public String getPromotionName() {
        return promotionName;
    }

    public void setPromotionName(String promotionName) {
        this.promotionName = promotionName;
    }

    public Integer getIsCheck() {
        isCheck = isCheck == null ? 0 : isCheck;
        return isCheck;
    }

    public void setIsCheck(Integer isCheck) {
        this.isCheck = isCheck;
    }

    public Integer getSkuId() {
        return skuId;
    }

    public void setSkuId(Integer skuId) {
        this.skuId = skuId;
    }

    public String getPromotionScript() {
        return promotionScript;
    }

    public void setPromotionScript(String promotionScript) {
        this.promotionScript = promotionScript;
    }

    @Override
    public String toString() {
        return "CartPromotionVo{" +
                "skuId=" + skuId +
                ", promotionId=" + promotionId +
                ", promotionType='" + promotionType + '\'' +
                ", promotionName='" + promotionName + '\'' +
                ", isCheck=" + isCheck +
                ", promotionScript='" + promotionScript + '\'' +
                '}';
    }


}
