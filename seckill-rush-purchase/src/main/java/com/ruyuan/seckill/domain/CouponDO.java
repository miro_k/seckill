package com.ruyuan.seckill.domain;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class CouponDO implements Serializable {

    private static final long serialVersionUID = 8587456467004980L;

    /**
     * 主键
     */
    private Integer couponId;

    /**
     * 优惠券名称
     */
    @NotEmpty(message = "请填写优惠券名称")
    private String title;

    /**
     * 优惠券面额
     */
    private Double couponPrice;

    /**
     * 优惠券门槛价格
     */
    private Double couponThresholdPrice;

    /**
     * 使用起始时间
     */
    private Long startTime;

    /**
     * 使用截止时间
     */
    private Long endTime;

    /**
     * 发行量
     */
    private Integer createNum;

    /**
     * 每人限领数量
     */
    private Integer limitNum;

    /**
     * 已被使用的数量
     */
    private Integer usedNum;

    /**
     * 已被领取的数量
     */
    private Integer receivedNum;

    /**
     * 店铺ID
     */
    private Integer sellerId;

    private String sellerName;

    /**
     * 优惠券类型，分为免费领取和活动赠送
     */
    private String type;

    /**
     * 使用范围，全品，分类，部分商品
     */
    private String useScope;


    /**
     * 范围关联的id
     * 全品或者商家优惠券时为0
     * 分类时为分类id
     * 部分商品时为商品ID集合
     */
    private String scopeId;


    /**
     * 店铺承担比例
     */
    private Integer shopCommission;

    /**
     * 范围描述
     */
    private String scopeDescription;

    /**
     * 活动说明
     */
    private String activityDescription;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getCouponId() {
        return couponId;
    }

    public void setCouponId(Integer couponId) {
        this.couponId = couponId;
    }

    public Double getCouponPrice() {
        return couponPrice;
    }

    public void setCouponPrice(Double couponPrice) {
        this.couponPrice = couponPrice;
    }

    public Double getCouponThresholdPrice() {
        return couponThresholdPrice;
    }

    public void setCouponThresholdPrice(Double couponThresholdPrice) {
        this.couponThresholdPrice = couponThresholdPrice;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public Integer getCreateNum() {
        return createNum;
    }

    public void setCreateNum(Integer createNum) {
        this.createNum = createNum;
    }

    public Integer getLimitNum() {
        return limitNum;
    }

    public void setLimitNum(Integer limitNum) {
        this.limitNum = limitNum;
    }

    public Integer getUsedNum() {
        return usedNum;
    }

    public void setUsedNum(Integer usedNum) {
        this.usedNum = usedNum;
    }

    public Integer getReceivedNum() {
        if (receivedNum == null) {
            receivedNum = 0;
        }
        return receivedNum;
    }

    public void setReceivedNum(Integer receivedNum) {
        this.receivedNum = receivedNum;
    }

    public Integer getSellerId() {
        return sellerId;
    }

    public void setSellerId(Integer sellerId) {
        this.sellerId = sellerId;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUseScope() {
        return useScope;
    }

    public void setUseScope(String useScope) {
        this.useScope = useScope;
    }

    public String getScopeId() {
        return scopeId;
    }

    public void setScopeId(String scopeId) {
        this.scopeId = scopeId;
    }

    public Integer getShopCommission() {
        return shopCommission;
    }

    public void setShopCommission(Integer shopCommission) {
        this.shopCommission = shopCommission;
    }

    public String getScopeDescription() {
        return scopeDescription;
    }

    public void setScopeDescription(String scopeDescription) {
        this.scopeDescription = scopeDescription;
    }

    public String getActivityDescription() {
        return activityDescription;
    }

    public void setActivityDescription(String activityDescription) {
        this.activityDescription = activityDescription;
    }


}
