package com.ruyuan.seckill.domain.enums;

/**
 * 商品类型枚举
 */
public enum GoodsType {

	/**
	 * 正常商品
	 */
	NORMAL("正常商品"), 
	/**
	 * 积分商品
	 */
	POINT("积分商品");

	private String description;

	GoodsType(String description) {
		this.description = description;

	}
}
